'use strict';

var gulp = require('gulp');
var minifyCss = require('gulp-minify-css');
var sass = require('gulp-sass');
var version = require('gulp-version-number');
var settings = require('./package.json');
var rev = require('gulp-rev');
var revManifest = require('gulp-revmanifest');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var scss_files = [
  'src/assets/scss/settings/settings.scss',
  'src/assets/scss/generic/generic.scss',
  'src/assets/scss/tools/tools.scss',
  'src/assets/scss/elements/elements.scss',
  'src/assets/scss/components/components.scss',
  'src/assets/scss/objects/objects.scss',
  'src/assets/scss/trumps/trumps.scss'
];
gulp.task('scss', function () {
  return gulp.src(scss_files)
    .pipe(sass({outputStyle: 'compressed', errLogToConsole: true}).on('error', sass.logError))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('src/assets/css/'))
});
var jsFiles = ['src/assets/js/general.js','src/assets/js/tabs.js'],
  jsDest = 'src/assets/js/build/';

gulp.task('scripts', function () {
  return gulp.src(jsFiles)
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(jsDest));
});
// gulp.task('watch', function () {
//     gulp.watch(["src/assets/scss/*/*.scss"], ['scss']);
// });


// .pipe(rev())
//     .pipe(revManifest({
//         base: ' ', // stripped from the destination path
//         merge: true // merge with the existing manifest (if one exists)
//     }))
gulp.task('default', ['scss', 'scripts']);
