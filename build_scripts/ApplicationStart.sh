#!/bin/bash
ln -s /usr/share/web/admin/dist/ /usr/share/nginx/html/admin
cp /usr/share/web/admin/build_scripts/admin.conf /etc/nginx/conf.d/
service nginx restart