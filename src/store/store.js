import Vue from 'vue'
import Vuex from 'vuex'
import * as getters from './getters'
import * as actions from './actions'
import mutations from './mutations'
import aat from './modules/aat/store'
import user_mngmt from './modules/users/store'

Vue.use(Vuex)
let jwt = localStorage.getItem('auth_token'),
  user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : {},
  user_roles = localStorage.getItem('user_roles') ? JSON.parse(localStorage.getItem('user_roles')) : [],
  token_expiry = localStorage.getItem('token_expiry') ? localStorage.getItem('token_expiry') : null

const state = {
  currentSubjectID: 'unanswered',
  asks: [],
  CURRENT_ASK: null,
  subjects: [],
  user: user,
  token_expiry: token_expiry,
  user_roles: user_roles,
  loading: false,
  authenticated: !!jwt,
  showModal: false,
  showAttachment: false,
  lessons: [],
  typing: false,
  typing_data: {},
  roles: [],
  users: {},
  user_details: {},
  with_roles: [],
  create_role: false,
  user_search: [],
  current_add: false,
  current_page_no: null,
  asks_link: '',
  current_fetched_pages: [1],
  loading_messages: false,
  detailRow: 0,
  data_table_loading: false,
  show_nav: false,
  show_dropdown: false
}
export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules: {
    aat: aat,
    user_mngmt: user_mngmt,
  },
})
