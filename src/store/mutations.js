import * as types from './mutation_types'

export default {

  [types.SET_ASKS](state, {list}) {
    state.asks = list
  },
  [types.SET_CURRENT_ASKS](state, {list}) {
    state.aat.current_asks = list
  },
  [types.SET_LESSONS](state, {list}) {
    state.lessons = list
  },
  [types.SET_USER](state, {list}) {
    state.user = list
  },
  [types.SET_TOKEN_EXPIRY](state, {list}) {
    state.token_expiry = list
  },
  [types.SET_USER_ROLES](state, {list}) {
    state.user_roles = list
  },
  [types.SET_STREAM](state, {list}) {
    state.stream = list
  },
  [types.SET_SUBJECTS](state, {list}) {
    state.subjects = list
  },
  [types.SWITCH_SUBJECT](state, {id}) {
    state.currentSubjectID = id
  },
  [types.SWITCH_QUESTION](state, {ask}) {
    state.CURRENT_ASK = ask
  },
  [types.TOGGLE_MODAL](state) {
    state.showModal = !state.showModal
  },
  [types.TOGGLE_PUSH](state) {
    state.showAttachment = !state.showAttachment
  },
  [types.LOADING](state) {
    state.loading = !state.loading
  },
  [types.TOGGLE_NAV](state) {
    state.show_nav = !state.show_nav
  },
  [types.TOGGLE_DROP](state) {
    state.show_dropdown = !state.show_dropdown
  },
  [types.SET_AUTH](state) {
    state.authenticated = !state.authenticated
  },
  [types.PUSH_ASK](state, {ask}) {
    let idx = state.asks.map(p => p.id).indexOf(ask.id)
    if (Number.isInteger(idx) && (idx > -1)) {
      if (ask.asker.consumer_id !== state.user.consumer_id) {
        return false
      }
      state.asks.splice(idx, 1)
      if (ask.asker.asks_count < 2) {
        state.asks.unshift(item)
      }
      else {
        state.asks.push(ask)
      }
    } else {
      if (ask.asker.asks_count < 2) {
        state.asks.unshift(item)
      }
      else {
        state.asks.push(ask)
      }
    }
  },
  [types.PUSH_PREV_Q](state, {ask}) {
    let idx = state.aat.prev_questions.map(p => p.id).indexOf(ask.id)
    if (Number.isInteger(idx) && (idx > -1)) {
      state.aat.prev_questions.splice(idx, 1)
      state.aat.prev_questions.push(ask)
    } else {
      state.aat.prev_questions.push(ask)
    }
  },
  [types.CREATE_USER](state, {new_user}) {
    state.users.push(new_user)
  },
  [types.SET_ROLES](state, {list}) {
    state.roles = list
  },
  [types.SET_ALL_ROLES](state, {list}) {
    state.user_mngmt.roles = list
    let roles_ = state.user_mngmt.roles
    state.user_mngmt.create_roles = []
    roles_.forEach(function (element) {
      let role_item = {value: element.name, label: element.display_name}
      state.user_mngmt.create_roles.push(role_item)
    })
  },
  [types.SET_USERS_WITH_ROLES](state, {list}) {
    state.with_roles = list
  },
  [types.TOGGLE_CREATE_ROLE](state, {microservice}) {
    if (microservice === 'close') {
      state.create_role = false
    } else {
      state.create_role = microservice
    }
  },
  [types.SET_USER_SEARCH](state, {list}) {
    state.user_search = list
  },
  [types.CURRENT_ADD](state, {list}) {
    state.current_add = list
  },
  [types.SET_USERS](state, {list}) {
    state.users = list
  },
  [types.SET_USER_DETAILS](state, {list}) {
    state.user_details = list
  },
  [types.ADD_USER_ROLE](state, {list}) {
    state.user_details.roles.push(list)
  },
  [types.SAVING_ROLE](state) {
    state.user_details.saving_role = !state.user_details.saving_role
  },
  [types.REMOVE_USER_ROLE](state, list) {
    let roles_ = state.user_details.roles,
      c_role = list.list
    let idx = roles_.map(p => p.id).indexOf(c_role.id)
    if (Number.isInteger(idx) && (idx > -1)) {
      roles_.splice(idx, 1)
    }
  },
  [types.TOGGLE_USER_SAVING](state) {
    state.user_mngmt.saving_detail = !state.user_mngmt.saving_detail
  },
  [types.SET_CURRENT_PAGE](state, {list}) {
    state.current_page_no = list
  },
  [types.SET_ASKS_LINK](state, {list}) {
    state.asks_link = list
  },
  [types.SET_CURRENT_FETCHED_PAGES](state, {list}) {
    state.current_fetched_pages = list
  },
  [types.LOADING_MESSAGES](state) {
    state.loading_messages = !state.loading_messages
  },
  [types.TOGGLE_DETAILS](state, {user_id}) {
    state.detailRow = user_id
  },
  [types.LOADING_DATA_TB](state) {
    state.data_table_loading = !state.data_table_loading
  },
  [types.CLEAR_DETAILS](state) {
    state.user_details = {}
  },
  [types.CLEAR_USERS](state) {
    state.users = {}
  },
  [types.LOADING_USER_DETAILS](state) {
    state.user_mngmt.loading_user = !state.user_mngmt.loading_user
  }

}

