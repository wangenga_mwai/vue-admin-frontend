import request from '../../../request'
import * as types from '../../mutation_types'

export const loadRoles = ({commit}, {microservice}) => {
  commit('LOADING')
  request.get(
    types.ROLES_BASE_URL + 'microservice/' + microservice + '/roles').then((response) => {
    commit('LOADING')
    commit('SET_ROLES', {list: response.data.data.results})
  }, (err) => {
    commit('LOADING')
    console.log(err)
  })
}
export const loadAllRoles = ({commit}) => {
  request.get(
    types.ROLES_BASE_URL + 'roles/all').then((response) => {
    commit('SET_ALL_ROLES', {list: response.data.data.results})
  }, (err) => {
    commit('LOADING')
    console.log(err)
  })
}
export const loadUsersWithRole = ({commit}, {userRole}) => {
  commit('LOADING')
  request.get(types.ROLES_BASE_URL + 'role/' + userRole + '/users').then((response) => {
    commit('LOADING')
    commit('SET_USERS_WITH_ROLES', {list: response.data.data.results})
  }, (error) => {
    commit('LOADING')
    console.log(error)
  })
}
export const createRole = ({commit}, {details}) => {
  commit('LOADING')
  request.post(types.ROLES_BASE_URL + 'roles/create', details).then(function () {
    return true
  }).catch(function (error) {
    console.log(error)
  })
}

// All User Management Actions
export const loadUsers = ({commit, dispatch}, {url, type}) => {
  request.get(url).then(response => {
    commit(types.SET_USERS, {list: response.data.data.results})
    if (type === 'search') {
      commit(types.LOADING_DATA_TB)
    } else {
      dispatch('loadAllRoles')
      if (response.data.data.results.current_page != 1) {
        commit(types.LOADING_DATA_TB)
      }
    }
  }).catch(error => {
    console.log(error)
  })
}
export const loadUserDetails = ({commit, dispatch}, {id}) => {
  commit(types.LOADING_USER_DETAILS)
  if (id !== 0) {
    request.get(types.USERS_BASE_URL + '/' + id).then(
      response => {
        dispatch('loadUserRoles', {id: id, user_details: response.data.data.results})
      }
    ).catch(
      error => {
        commit(types.LOADING_USER_DETAILS)
        console.log(error)
      }
    )
  }
  else {
    commit(types.LOADING_USER_DETAILS)
  }
}
export const loadUserRoles = ({commit}, {id, user_details}) => {
  request.get(types.ROLES_BASE_URL + 'user/' + id + '/roles').then(function (response) {
    commit(types.LOADING_USER_DETAILS)
    user_details.roles = response.data.data.results
    commit(types.SET_USER_DETAILS, {list: user_details})
  })
}
export const createUser = ({dispatch}, {new_user}) => {
  request.post(types.USERS_BASE_URL, new_user).then((result) => {
    let role_details = new_user.roles,
      id = result.data.data.results.id,
      details = [id, role_details]
    dispatch('saveUserRole', {'details': details})
    return true
  }).catch(error => {
    console.log(error)
  })
}
export const editUserDetails = ({commit}, {user_id, updated_detail}) => {
  request.post(types.USERS_BASE_URL + '/' + user_id, updated_detail).then((result) => {
    commit(types.TOGGLE_USER_SAVING)
    return result
  }).catch(error => {
    commit(types.TOGGLE_USER_SAVING)
    console.error(error)
  })
}
export const searchUsers = ({commit}, {search}) => {
  if (search.length <= 0) {
    commit('SET_USER_SEARCH', {list: []})
  } else {
    request.get(types.ROLES_BASE_URL + 'users?take=8&search=' + search).then(resp => {
      commit('SET_USER_SEARCH', {list: resp.data.data.results.data})
    })
  }
}
export const saveUserRole = ({commit}, {details}) => {
  let user_id = details[0], request_details = details[1], role_ = details[2]
  commit(types.SAVING_ROLE)
  request.post(types.ROLES_BASE_URL + 'user/' + user_id + '/attach', request_details).then(function () {
    commit(types.SAVING_ROLE)
    if (role_) {
      commit(types.ADD_USER_ROLE, {list: role_})
    }
    return true
  }).catch(function (error) {
    console.log(error)
  })

}
export const detachUserRole = ({commit}, {details}) => {
  let user_id = details[0], request_details = details[1], role_ = details[2]
  request.post(types.ROLES_BASE_URL + 'user/' + user_id + '/detach', request_details).then(function () {
    commit(types.REMOVE_USER_ROLE, {list: role_})
    return true
  }).catch(function (error) {
    console.log(error)
  })
}
export const toggleDetailRow = ({commit, dispatch}, {id}) => {
  commit(types.TOGGLE_DETAILS, {user_id: id})
  dispatch('loadUserDetails', {id: id})
}
