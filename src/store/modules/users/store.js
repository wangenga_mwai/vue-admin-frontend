import *  as getters from './getters'
import *  as actions from './actions'

const users = {
  state: {
    loading_user: false,
    saving_detail:false,
    saving_role: false,
    roles:[]
  },
  actions: actions,
  getters: getters,
}

export default users
