export const getRoles = (state, getters, rootState) => {
  return rootState.roles
}
export const getUsersWithRole = (state, getters, rootState) => {
  return rootState.with_roles
}
export const userSearch = (state, getters, rootState) => {
  return rootState.user_search
}
export const getUsers = (state, getters, rootState) => {
  return rootState.users
}
export const getDetailRow = (state, getters, rootState) => {
  return rootState.detailRow
}
export const getDetailedUser = (state, getters, rootState) => {
  return rootState.user_details
}
export const getDataTableLoadStatus = (state, getters, rootState) => {
  return rootState.data_table_loading
}
export const getLoadingUser = (state, getters, rootState) => {
  return rootState.user_mngmt.loading_user
}
export const getSavingDetail = (state, getters, rootState) => {
  return rootState.user_mngmt.saving_detail
}
export const getSavingRole = (state, getters, rootState) => {
  return rootState.user_mngmt.saving_role
}
export const getAllRoles = (state, getters, rootState) => {
  return rootState.user_mngmt.roles
}
export const getCreateRoles = (state, getters, rootState) => {
  return rootState.user_mngmt.create_roles
}
