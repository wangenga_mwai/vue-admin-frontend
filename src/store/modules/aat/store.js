import *  as getters from './getters'
import *  as actions from './actions'
const aat = {
  state: {
    prev_questions : [],
    loading_msg: false
  },
  actions: actions,
  getters: getters,
}

export default aat
