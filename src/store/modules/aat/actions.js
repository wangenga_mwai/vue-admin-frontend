import axios from 'axios'
import * as types from '../../mutation_types'
import store from '../../store'
import request from '../../../request'


export const loadAsks = ({commit}, {user_id, dates, cat}) => {
  commit(types.LOADING)
  let to = dates.to,
    from = dates.from,
    url_ = types.ASKS_BASE_URL + '/responder/' + user_id + '?limit=80&orderBy=id&sortedBy=asc&from=' + from + '&to=' + to + '&searchFields=type&search=' + cat
  request.get(url_).then((response) => {
    let asks = response.data.data.results.asks.data
    asks.forEach(function (item, index, array) {
      if (item.asker.asks_count < 2) {
        asks.splice(index, 1)
        asks.unshift(item)
      }
    })
    commit(types.SET_ASKS, {list: asks})
    commit(types.SET_ASKS_LINK, {list: url_})
    commit(types.SET_STREAM, {list: response.data.data.results.asks.data})
    commit(types.SET_SUBJECTS, {list: response.data.data.results.subjects})
    commit(types.SET_CURRENT_PAGE, {list: response.data.data.results.asks.current_page})
    commit(types.SET_CURRENT_FETCHED_PAGES, {list: []})
  }, (error) => {
    commit(types.LOADING)
    console.log(error)
  })
  request.get(url_ + '&complete=1').then((response) => {
    commit(types.LOADING)
    var asks_ = store.state.asks.concat(response.data.data.results.asks.data)
    commit(types.SET_ASKS, {list: asks_})

  }, (error) => {
    commit(types.LOADING)
    console.log(error)
  })
}
export const appendAsks = ({commit}, {page, asks, url_, pages_array}) => {
  commit(types.LOADING_MESSAGES)
  let next_page = page + 1
  pages_array.push(next_page)
  request.get(url_ + '&page=' + page).then((response) => {
    var combined_asks = asks.concat(response.data.data.results.asks.data)
    commit(types.SET_CURRENT_PAGE, {list: next_page})
    commit(types.SET_CURRENT_FETCHED_PAGES, {list: pages_array})
    commit(types.SET_ASKS, {list: combined_asks})
  }, (error) => {
    commit(types.LOADING)
    console.log(error)
  })
  request.get(url_ + '&complete=1' + '&page=' + page).then((response) => {
    commit(types.LOADING_MESSAGES)
    var asks_ = store.state.asks.concat(response.data.data.results.asks.data)
    commit(types.SET_ASKS, {list: asks_})
  }, (error) => {
    commit(types.LOADING)
    console.log(error)
  })
}
export const loadLessons = ({commit}, {type, query}) => {
  request.get(types.ASKS_BASE_URL + '/search/' + type + '?query=' + query)
    .then(
      response => {
        commit(types.SET_LESSONS, {list: response.data.data.results})
      }
    )
    .catch(error => console.log(error))
}

export const sendReply = ({state}, {reply}) => {
  request.post(types.ASKS_BASE_URL + '/replies/', reply)
    .then(() => {
      return true
    })
    .catch(error => {
      console.log(error)
    })
}
export const pushContent = ({state}, {content}) => {
  request.post(types.ASKS_BASE_URL + '/push/', content)
    .then(() => {
      return true
    })
    .catch(error => {
      console.log(error)
    })
}
export const switchThread = ({commit}, payload) => {
  commit(types.SWITCH_SUBJECT, payload)
}
export const switchQuestion = ({commit}, payload) => {
  commit(types.SWITCH_QUESTION, payload)
}
export const loadPrevLessons = ({commit}, {user_id}) => {
  store.state.aat.loading_msg = true
  request.get(types.ASKS_BASE_URL + '/asks?limit=50&orderBy=id&sortedBy=desc&search=' + user_id + '&searchFields=user_id')
    .then((response) => {
      store.state.aat.prev_questions = response.data.data.results.data.reverse()
      store.state.aat.loading_msg = false
    }, (error) => {
      console.log(error)
      store.state.aat.loading_msg = false
    })
}
