export const getAsks = (state, getters, rootState) => rootState.asks
export const getSubjects = (state, getters, rootState) => rootState.subjects
export const getUser = (state, getters, rootState) => rootState.user
export const getLessons = (state, getters, rootState) => rootState.lessons

export const currentSubject = rootState => {
  return rootState.currentSubjectID
}

export const currentAsks = (state, getters, rootState) => {
  const subject = currentSubject(rootState)
  if (subject === 'all') {
    return rootState.asks
  } else if (subject === 'unanswered') {
    return unanswered(rootState)
  } else if (subject === 'answered') {
    return answered(rootState)
  } else if (subject === 'unanswered-help') {
    return unanswered_help(rootState)
  } else if (subject === 'answered-help') {
    return answered_help(rootState)
  } else if (subject === 'unanswered-feedback') {
    return unanswered_help(rootState)
  } else if (subject === 'answered-feedback') {
    return answered_help(rootState)
  }
  else {
    return rootState.asks.filter(function (ask) {
      return ask.subject_id === subject
    })
  }
}

export const currentLessons = (state, getters, rootState) => {
  return rootState.lessons
}
export const currentQuestion = (state, getters, rootState) => {
  return rootState.CURRENT_ASK
}
let unanswered = function (rootState) {
  return rootState.asks.filter(function (ask) {
    return ask.type === 'Ask-a-Teacher' && ask.replies.length < 1
  })
}
let answered = function (rootState) {
  return rootState.asks.filter(function (ask) {
    return ask.type === 'Ask-a-Teacher' && ask.replies.length > 0
  })
}
let unanswered_help = function (rootState) {
  return rootState.asks.filter(function (ask) {
    return ask.type !== 'Ask-a-Teacher' && ask.replies.length < 1
  })
}
let answered_help = function (rootState) {
  return rootState.asks.filter(function (ask) {
    return ask.type !== 'Ask-a-Teacher' && ask.replies.length > 0
  })
}
let prevQuestions = function (rootState) {
  return rootState.aat.prev_questions
}
