import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/components/auth/Login.vue'
import Asks from '@/components/aat/Asks.vue'
import Dash from '@/components/dashboard/Dashboard.vue'
import Roles from '@/components/roles/Roles.vue'
import UserDetails from '@/components/Users/Management/UserDetails.vue'
import CreateUser from '@/components/Users/Management/CreateUser.vue'
import UserList from '@/components/Users/Management/UserList.vue'
import RolesUsers from '@/components/roles/List.vue'
import NotFound from '@/components/errors/NotFound.vue'
import dashPerimeter from '@/perimeters/DashPerimeter'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {title: 'Login | Dashboard'}
  },
  {
    path: '/',
    name: 'home',
    component: Dash,
    meta: {title: 'Dashboard', requiresAuth: true}
  },
  {
    path: '/asks',
    component: Asks,
    name: 'asks',
    meta: {
      title: 'Ask a teacher | Dashboard',
      requiresAuth: true,
      perimeter: dashPerimeter,
      perimeterAction: 'replyAsk'
    }
  },
  {
    path: '/roles/:microservice',
    component: Roles,
    name: 'roles',
    meta: {
      title: 'Role Management | Dashboard',
      requiresAuth: true,
      perimeter: dashPerimeter,
      perimeterAction: 'viewRoles'
    },
    props: true
  },
  {
    path: '/roles/:microservice/:userRole',
    component: RolesUsers,
    name: 'RolesUsers',
    meta: {
      title: 'Role Management | Users',
      requiresAuth: true,
      perimeter: dashPerimeter,
      perimeterAction: 'viewRoles'
    },
    props: true
  },
  {
    path: '/users',
    component: UserList,
    name: 'users',
    meta: {
      title: 'Users | Dashboard',
      requiresAuth: true,
      perimeter: dashPerimeter,
      perimeterAction: 'viewRoles'
    },
    props: true
  },
  {
    path: '/user/:id',
    component: UserDetails,
    name: 'user_details',
    meta: {
      title: 'User Details',
      requiresAuth: true,
      perimeter: dashPerimeter,
      perimeterAction: 'viewRoles'
    },
    props: true
  },
  {
    path: '/users/create',
    component: CreateUser,
    name: 'create_user',
    meta: {
      title: 'Create | New User',
      requiresAuth: true,
      perimeter: dashPerimeter,
      perimeterAction: 'viewRoles'
    },
    props: true
  },
  {
    path: '/*',
    name: '404',
    component: NotFound,
    meta: {title: '404'}
  }
]

export default new VueRouter({
  routes,
  mode: 'history'
})
