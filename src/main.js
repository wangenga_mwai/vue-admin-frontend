import Vue from 'vue'
import App from './App.vue'
import store from './store/store'
import router from './router'
import Vue2Filters from 'vue2-filters'
import VSvg from 'v-svg-directive'
import truncate from 'vue-truncate'
import Scripts from './assets/js/build/scripts.min.js'
import Style from './assets/css/style.min.css'
import Notifications from 'vue-notification'
import VeeValidate from 'vee-validate'
import auth from './auth'
import vSelect from 'vue-select'
import VueKindergarten from 'vue-kindergarten'
import focus from 'vue-focus'
import VModal from 'vue-js-modal'
import { createSandbox } from 'vue-kindergarten'
import RouteGoverness from '@/governesses/RouteGoverness'
import child from './child'
import vOffline from 'v-offline'

Vue.config.productionTip = false

Vue.use(Vue2Filters)
Vue.use(Notifications)
Vue.use(VeeValidate)
Vue.use(truncate)
Vue.use(Scripts)
Vue.use(Style)
Vue.use(Notifications)
Vue.use(VeeValidate)
Vue.use(truncate)
Vue.use(VSvg, {
  path: '/static/svg-icons/eneza-icons.svg',
  class: 'icon',
})
Vue.use(Scripts)
Vue.use(Style)
Vue.use(vSelect)
Vue.use(VueKindergarten)
Vue.use(focus)
Vue.use(VModal)
Vue.component('v-select', vSelect)
Vue.component('detectNetwork', vOffline)

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  if (to.matched.some(record => record.meta.requiresAuth)) {
    const perimeter = to.meta.perimeter
    const Governess = to.meta.governess || RouteGoverness
    const action = to.meta.perimeterAction || 'route'
    if (!auth.checkAuth()) {
      next({
        path: '/login',
        query: {redirect: to.fullPath},
      })
    }
    if (perimeter) {
      const sandbox = createSandbox(child(), {
        governess: new Governess(),

        perimeters: [
          perimeter,
        ],
      })
      return sandbox.guard(action, { to, from, next })
    } else {
      next()
    }
  } else {
    next()
  }
})
export const vm = new Vue({
  router,
  store,
  template: '<App/>',
  components: {App},
}).$mount('#application')

