import Pusher from 'pusher-js'
import axios from 'axios'

// Pusher.logToConsole = false

export const pusher = new Pusher('5113318df316bf7a2e4d', {
  cluster: 'eu',
  encrypted: true,
  authorizer: function (channel, options) {
    return {
      authorize: function (socketId, callback) {
        axios.post('http://vrata.enezaeducation.com/v1/asks/pusher/auth', {
          channel_name: channel.name,
            socket_id: socketId
        })
          .then(function (response) {
            callback(false, response.data.data.results)
          })
          .catch(function (error) {
            console.log(error)
          })
      }
    }
  }
})

export const asks = pusher.subscribe('asks')
export const replies = pusher.subscribe('replies')
export const replying = pusher.subscribe('private-replying')
