window.$ = window.jQuery = require('jquery');
$(document).on('click', '.aat-chat-tabs a', function (e) {
  e.preventDefault();
  var tabNav = this;
  $(tabNav).siblings().removeClass('active');
  $(tabNav).addClass('active');
  question_scroll();

});
$(document).on('click', '.aat-chat-tabs a[data-toggle="modal"]', function (e) {
  e.preventDefault();
  $('.aat-sidebar').removeClass('visible');

  var selector = $(this), slider_id = selector.attr('data-target');
  $(slider_id).addClass('visible');
});

$(document).on('click', '.aat-sidebar .close', function (e) {
  e.preventDefault();
  var selector = $(this);
  selector.closest('.aat-sidebar').removeClass('visible');
});
$(document).on('click', '.main-header,.aat-chat-tabs a[data-dismiss="modal"]', function (e) {
  e.preventDefault();
  $('.aat-sidebar').removeClass('visible');
});
$(document).on('click', '.aat-type-switch p', function (e) {

  var clicked_ = $(this), parent = clicked_.closest('.aat-type-switch');

  if (parent.hasClass('open')) {
    parent.removeClass('open');
    parent.siblings().removeClass('aat-type-tabs-open');
  } else {
    parent.addClass('open');
    parent.siblings().addClass('aat-type-tabs-open');
  }
  var clicked_sibling = clicked_.siblings();
  clicked_sibling.removeClass('active');
  if (!clicked_.hasClass('active')) {
    question_scroll();
    clicked_.addClass('active');
  }
  clicked_sibling.each(function () {
    var item = $(this), hide_tab = item.data('tab'), tab_h = $('.' + hide_tab);
    tab_h.hide();
  })

  var tab_class = clicked_.data('tab'), tab_ = $('.' + tab_class)

  tab_.show();
  tab_.first().addClass('active');


});
$(document).on('click', '.aat-sidebar .btn-select-subject', function () {
  var clicked_ = $(this);
  if (!clicked_.hasClass('active')) {
    $('.aat-sidebar .btn-select-subject').removeClass('active');
    clicked_.addClass('active');
  }

});

function question_scroll() {
  $(document).find('.questions-holder').animate({
    scrollTop: 0
  }, 500)
}

// (function() {
//   var s = document.createElement("script");
//   s.type = "text/javascript";
//   s.async = true;
//   s.src = '//api.usersnap.com/load/cf7e76d1-2a2d-4ad4-b63c-75d91eea0999.js';
//   var x = document.getElementsByTagName('script')[0];
//   x.parentNode.insertBefore(s, x);
// })();
