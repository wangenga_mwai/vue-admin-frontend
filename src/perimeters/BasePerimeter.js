import {Perimeter} from 'vue-kindergarten'
import store from '../store/store'

export default class BasePerimeter extends Perimeter {
  isAdmin() {
    let roles_ = store.state.user_roles,
      ret_ = roles_.filter(function (el) {
        return el.name === 'mwalimoo.admin'
      })
    if (ret_.length > 0) {
      return ret_
    }
    return false
  }

  isResponder() {
    let roles_ = store.state.user_roles
    var ret_ = roles_.filter(function (el) {
      return el.name === 'mwalimoo.admin' ||
        el.name === 'mwalimoo.ask_responder' ||
        el.name === 'mwalimoo.responder_admin'

    })
    if (ret_.length > 0) {
      return ret_
    }
  }
}
