import BasePerimeter from './BasePerimeter'
import { HeadGoverness } from 'vue-kindergarten'
export default new BasePerimeter({
  purpose: 'aat',
  can: {
    replyAsk() {
      return this.isResponder()
    }
  },
  governess: HeadGoverness
})
