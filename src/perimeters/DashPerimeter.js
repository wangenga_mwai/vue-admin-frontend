import BasePerimeter from './BasePerimeter'
import { HeadGoverness } from 'vue-kindergarten'
export default new BasePerimeter({
  purpose: 'dashboard',
  can: {
    viewRoles() {
      return this.isAdmin()
    },
    replyAsk() {
      return this.isResponder()
    }
  },
  governess: HeadGoverness
})
