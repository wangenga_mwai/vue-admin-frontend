/**
 * Created by bix on 12/09/2017.
 */

import moment from 'moment'

export const globals = {
  BASE_API_URL: 'http://vrata-test.enezaeducation.com/v1/asks-test',
  USERS_BASE_URL: 'http://vrata-test.enezaeducation.com/v1/hydra/v1/users',
  ROLES_BASE_URL: 'http://vrata-test.enezaeducation.com/v1/hydra/v1/',
}

const helpers = {
  getTimeAgo: function (date) {
    return moment.utc(date).fromNow()
  },
}
export default helpers
