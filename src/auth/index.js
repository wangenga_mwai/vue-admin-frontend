import router from '../router'
import axios from 'axios'
import store from '../store/store'
import * as types from '../store/mutation_types'
import moment from 'moment'
// endpoints
const API_URL = ' http://vrata.enezaeducation.com/'
const LOGIN_URL = API_URL + 'ingia/web'

export default {
  user: {
    authenticated: false
  },
  login(context, credentials) {
    let inst = this
    store.commit('LOADING')
    axios.post(LOGIN_URL, credentials)
      .then(function (response) {
        let expiry_ = response.data.expires_in,
          expires_in = moment().seconds(expiry_).utc()
        localStorage.setItem('auth_token', response.data.access_token)
        localStorage.setItem('user', JSON.stringify(response.data.user))
        localStorage.setItem('token_expiry', expires_in)
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('auth_token')
        inst.getUserRoles(response.data.user.id)
        store.commit('SET_USER', {list: response.data.user})
        store.commit('SET_TOKEN_EXPIRY', {list: localStorage.getItem('token_expiry')})
        store.commit('SET_AUTH')
        store.commit('LOADING')
        router.push({name: 'home'})
      })
      .catch(function (error) {
        store.commit('LOADING')
        context.error = error.message
        console.log(error)
      })
  },
  logout() {
    localStorage.removeItem('auth_token')
    localStorage.removeItem('user')
    localStorage.removeItem('user_roles')
    localStorage.removeItem('token_expiry')
    store.commit('SET_USER', {})
    store.commit('SET_AUTH')
    router.push('login')
  },

  checkAuth() {

    let jwt = localStorage.getItem('auth_token')
    if (jwt) {
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('auth_token')
    }
    return !!jwt
  },

  getAuthHeader() {
    return {
      'Authorization': 'Bearer ' + localStorage.getItem('auth_token')
    }
  },
  getUserRoles(id) {
    axios.get(types.ROLES_BASE_URL + 'user/' + id + '/roles').then(function (response) {
      localStorage.setItem('user_roles', JSON.stringify(response.data.data.results))
      store.commit('SET_USER_ROLES', {list: response.data.data.results})
    })
  }
}
