var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  P_KEY:'5113318df316bf7a2e4d',
  P_SECRET: '31b0b20203407f6e284e',
  P_APP_ID: '317617',
  P_CLUSTER: "eu"

})
